﻿using AutoMapper;
using LibraryApi.Controllers;
using LibraryApi.Models.Dtos;
using LibraryApi.Models.Entities;
using LibraryApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LibraryApi.Test.ControllerTests
{
    public class AuthorControllerTests
    {
        private readonly Mock<IAuthorService> _mockAuthorService;
        private readonly AuthorsController _authorsController;
        private readonly Mock<IBookService> _mockBookService;
        private readonly Mock<IMapper> _mockMapper;

        public AuthorControllerTests()
        {
            _mockAuthorService = new Mock<IAuthorService>();
            _mockBookService = new Mock<IBookService>();
            _mockMapper = new Mock<IMapper>();
            _authorsController = new AuthorsController(_mockAuthorService.Object, _mockBookService.Object, _mockMapper.Object);
        }

        [Fact]
        public void GetAuthors_ActionExecutes_ReturnsIActionResult()
        {
            Assert.IsType<Task<IActionResult>>(_authorsController.GetAuthors());
        }
       
        [Fact]
        public async void Get_WhenCalled_ReturnsOkResult()
        {
            var okResult = await _authorsController.GetAuthors();

            Assert.IsType<OkObjectResult>(okResult as OkObjectResult);
        }

        [Fact]
        public async void GetAuthorById_UnknownGuidPassed_ReturnsNoContentResult()
        {
            var notFoundResult = await _authorsController.GetSingleAuthor(Guid.Parse("1267b0d2-ec81-4865-f014-08d9a5c0ea78"));

            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public async void GetAuthorById_ExistingGuidPassed_ReturnsOkResult()
        {
            var testGuid = new Guid("cef75f5f-2d09-454f-f013-08d9a5c0ea78");

            var okResult = await _authorsController.GetSingleAuthor(testGuid);

            Assert.IsType<OkObjectResult>(okResult as OkObjectResult);
        }
   
        [Fact]
        public async void Add_InvalidObjectPassed_ReturnsBadRequest()
        {
            var authorMissing = new AuthorForCreationDto()
            {
                UserId = Guid.NewGuid().ToString()
            };
            _authorsController.ModelState.AddModelError("Email", "Required");

            var badResponse = await _authorsController.AddSingleAuthor(authorMissing);

            Assert.IsType<BadRequestObjectResult>(badResponse);
        }
    }
}
