﻿using FluentAssertions;
using LibraryApi.Test.TestUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LibraryApi.Test.ControllerTests
{
    public class AuthorsControllerIntegrationTests : IClassFixture<CustomApplicationFactory<Startup>>
    {
        private readonly HttpClient _httpClient;

        public AuthorsControllerIntegrationTests(CustomApplicationFactory<Startup> factory)
        {
            _httpClient = factory.CreateClient();
        }

        [Fact]
        public async Task GetAuthors_ReturnsAllAuthors()
        {
            string url = Endpoints.Authors;

            var res = await _httpClient.GetAsync(url);

            res.EnsureSuccessStatusCode();

            res.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}
